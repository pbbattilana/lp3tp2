#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <ctype.h>
#define MAX 30
#define LONG 50
#define SLEEP 2
#define gotoxy(x,y) printf("\033[%d;%dH", (x), (y))
#define clear() printf("\033[H\033[J")

void* iniciar(void *str);
void * leer (void *str);

/**Definimos el auto. Dirección:
 * 0 para derecha
 * 1 para izquierda
*/
typedef struct{
	char nombre[6];
	int direccion;
	int color;
}car;

typedef car tipoDato;

//Definimos la estructura puente
typedef struct{
	car primero;
	car segundo;
	car tercero;
	int cantidad;
	int cruce;
	int salida;
}Puente;

//Definimos la estructura cola
typedef struct{
	tipoDato listaCola[MAX];
	int head, tail, cantidad;
}Cola;

//Definimos los comportaientos del puente
void crearPuente(Puente *p);
void ingresaIzq(Puente *p);
void ingresaDer(Puente *p);
void avanzaIzq(Puente* p);
void avanzaDer(Puente* p);
void showBridge(Puente p);
int getCruce(Puente p);
void aumentarCruce(Puente *p);
void resetCruce(Puente *p);
int comprobarAuto(char nombre[]);

//Definimos los comportamientos de la cola
int sigte(int n);
void crearCola(Cola *c);
void encolar(Cola* c, tipoDato nuevo);
tipoDato decolar(Cola* c);
int getHead(Cola c);
int isEmpty(Cola c);
int isFull(Cola c);
int getCantidad(Cola c);
void listar(Cola c);
