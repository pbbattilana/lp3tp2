#include "header.h"

pthread_mutex_t msg_mutex = PTHREAD_MUTEX_INITIALIZER;
void (*ptr_avanza[])(Puente*) = {ingresaIzq, ingresaDer};

car nulo;
Puente p;
Cola colaIzq;
Cola colaDer;

int q=0;
int primerAuto=0;
char *autoI = "  -->  ";
char *autoD = "  <--  ";
char *autoN = "       ";
int quit = 0;
int start = 0;
int function = 0;

// 31 rojo | 21 verde | 33 amarillo | 34 azul | 35 lila | 36 celeste | 37 blanco
char * colores[7]= {"\033[0;31m","\033[0;37m","\033[0;34m","\033[0;36m","\033[0;37m","\033[0;31m","\033[0;33m"};
char * nombreAuto[100];
char * argumentos[10];

void crearCola(Cola *c){
	c->head = 0;
	c->tail = MAX-1;
	c->cantidad = 0;
}

void encolar(Cola *c, tipoDato nuevo){
	if(isFull(*c)){
		printf("La cola está llena\n");
		exit(1);
	}else{
		c->tail = sigte(c->tail);
		c->listaCola[c->tail] = nuevo;
		(c->cantidad)++;
	}
}

tipoDato decolar(Cola *c){
	tipoDato temp;

	if(isEmpty(*c)){
		printf("La cola está vacia\n");
		exit(1);
	}else{
		temp = c->listaCola[c->head];
		c->head = sigte(c->head);
		(c->cantidad)--;
		return temp;
	}
}

void listar(Cola c){
	if(isEmpty(c) == 1){
		printf("Vacio\033[K\n");
	}else{
		int z;
		for(z = c.head; z <= c.tail; z++ ){
			printf("%s%s", c.listaCola[z].nombre, (z == c.tail)?"\033[K\n":" ");
		}
	}
}

int isEmpty(Cola c){
	return (c.head == sigte(c.tail));
}

int isFull(Cola c){
	return (c.head == sigte(sigte(c.tail)));
}

int getHead(Cola c){
	return c.head;
}

int getCantidad(Cola c){
	return c.cantidad;
}

int sigte(int n){
	return ((n+1) % MAX);
}

void *iniciar(void *str){
    while (start == 0)
        sleep(2);

    while (quit == 0){
        sleep(1);
        (*ptr_avanza[function])(&p);
    }
    pthread_exit(0);
}

void crearPuente(Puente *p){
	p->cantidad = p->cruce = 0;
	p->primero = p->segundo = p->tercero = nulo;
}

void ingresaIzq(Puente *p){
	if(getCantidad(colaIzq) == 0 && p->cantidad == 0){
		function = 1; 
		showBridge(*p);
		return;
	}
    //Avanzar autos
	avanzaDer(p);
	if(getCantidad(colaIzq) == 0 && getCantidad(colaDer) == 0)
		showBridge(*p);

    //Para ir hacia la derecha decolar izquierda
	if(getCantidad(colaDer) == 0){
		if(!isEmpty(colaIzq)){
			pthread_mutex_lock(&msg_mutex);
                p->primero = decolar(&colaIzq);
                (p->cantidad)++;
                showBridge(*p);
			pthread_mutex_unlock(&msg_mutex);
		}	
	}else{
        //Autos en espera
		int autos = getCantidad(colaIzq) + p->cantidad;
		if(autos >= 4 && quit == 0){
			int i = 0;
			while(i + (p->cantidad) < 4){
                //Avanzar los autos
				avanzaDer(p);
				if(i < 3){
					if(!isEmpty(colaIzq)){
						pthread_mutex_lock(&msg_mutex);
                            p->primero = decolar(&colaIzq);
                            (p->cantidad)++;
						pthread_mutex_unlock(&msg_mutex);
					}
				}
				i += (p->salida);
				showBridge(*p);
			}
            //Habilitar el otro sentido vaciando el puente
			while(p->cantidad > 0){
				avanzaDer(p);
				showBridge(*p);
			}
		}else{
			int i = 0;
			while(i+(p->cantidad) < autos  && quit == 0){
                //Avanzar los autos
				avanzaDer(p);
				if(!isEmpty(colaIzq)){
					pthread_mutex_lock(&msg_mutex);
                        p->primero = decolar(&colaIzq);
                        (p->cantidad)++;
					pthread_mutex_unlock(&msg_mutex);
				}
				i += (p->salida);
				showBridge(*p);
			}
            //Habilitar el otro sentido vaciando el puente
			while(p->cantidad > 0){
				avanzaDer(p);
				showBridge(*p);
			}
		}
		resetCruce(p);
        //Cambiamos el sentido
		function = 1; 
	}
}

void avanzaDer(Puente* p){
	p->salida = 0;
	if((p->tercero).direccion != -1){
		(p->cantidad)--;
		p->salida = 1;
	}
	p->tercero = nulo;
	p->tercero = p->segundo;
	p->segundo = p->primero;
	p->primero = nulo;
}

void ingresaDer(Puente *p){
	if(getCantidad(colaDer) == 0 && p->cantidad == 0){
		function = 0; 
		showBridge(*p);
		return;
	}
    //Avanzar los autos
	avanzaIzq(p);
	if(getCantidad(colaIzq) == 0 && getCantidad(colaDer) == 0)
		showBridge(*p);

	//Para ir hacia la izquierda decolar derecha
	if(getCantidad(colaIzq) == 0){
        //Sin espera del otro lado
		if(!isEmpty(colaDer)){
			pthread_mutex_lock(&msg_mutex);
                p->tercero = decolar(&colaDer);
                (p->cantidad)++;
                showBridge(*p);
			pthread_mutex_unlock(&msg_mutex);
		}
		
	}else{
        //Autos en espera
		int autos = getCantidad(colaDer) + p->cantidad;
		if(autos >= 4){
			int i = 0;
			while(i+(p->cantidad) < 4 && quit == 0){
				avanzaIzq(p);
				if(i < 3){
					if(!isEmpty(colaDer)){
						pthread_mutex_lock(&msg_mutex);
                            p->tercero = decolar(&colaDer);
                            (p->cantidad)++;
						pthread_mutex_unlock(&msg_mutex);
					}
				}
				i += (p->salida);
				showBridge(*p);
			}
            //Habilitar el otro sentido vaciando el puente
			while(p->cantidad > 0){
				avanzaIzq(p);
				showBridge(*p);
			}
		}else{
			int i = 0;
            //Avanzar autos en espera
			while(i+(p->cantidad) < autos  && quit == 0){
				avanzaIzq(p);
				if(!isEmpty(colaDer)){
					pthread_mutex_lock(&msg_mutex);
                        p->tercero = decolar(&colaDer);
                        (p->cantidad)++;
					pthread_mutex_unlock(&msg_mutex);
				}
				i += (p->salida);
				showBridge(*p);
			}
            //Habilitar el otro sentido vaciando el puente
			while(p->cantidad > 0){ 
				avanzaIzq(p);
				showBridge(*p);
			}
		}
		resetCruce(p);
        //Cambiar el sentido
		function = 0;
	}
	
}

void avanzaIzq(Puente* p){
	p->salida = 0;
	if((p->primero).direccion != -1){
		(p->cantidad)--;
		p->salida = 1;
	}
	p->primero = nulo;
	p->primero = p->segundo;
	p->segundo = p->tercero;
	p->tercero = nulo;
}

int getCruce(Puente p){
	return p.cruce;
}

void aumentarCruce(Puente *p){
	(p->cruce)++;
}

void resetCruce(Puente *p){
	p->cruce = 0;
}

void showBridge(Puente p){
    //Cursor
	printf("\033[s");
	gotoxy(1,1);

	char * autoNulo[4]={"                       ","                       ", "                       ", "                       "};
	char * autoIzq[4] ={"       ╔════╦═══╗      ","  ┌────╬────╬───╬───┐  ", "  )       - ║  <<<< [  ", "  └──(0)───────(0)──┘  "};
	char * autoDer[4] ={"      ╔══╦═════╗       ","  ┌───╬──╬─────╬────┐  ", "  ] >>>> ║  -       (  ", "  └──(0)───────(0)──┘  "};
	char * finColor = "\033[0m";
	int colorAuto[3];
	int j, i;

	for(i=0; i<15; i++){
		if(i!=17 && i!=16)
			printf("\33[K\n");
	}

	gotoxy(1,1);
	printf("\33[2K");


	printf("           %6s                     %6s                     %6s          En el puente: %2d  \n", p.primero.nombre, p.segundo.nombre, p.tercero.nombre, p.cantidad);

    //De izquierda a derecha
	if(function == 0){
		for(i=0; i<4; i++){
			j=0;
			if(p.primero.direccion == -1)
				printf("    %s%6s%s", colores[p.primero.color], autoNulo[i], finColor);
			else
				printf("    %s%6s%s", colores[p.primero.color], autoDer[i], finColor);
			if(p.segundo.direccion == -1 )
				printf("    %s%6s%s", colores[p.segundo.color], autoNulo[i], finColor);
			else
				printf("    %s%6s%s", colores[p.segundo.color], autoDer[i], finColor);
			if(p.tercero.direccion == -1 )
				printf("    %s%6s%s", colores[p.tercero.color], autoNulo[i], finColor);
			else
				printf("    %s%6s%s", colores[p.tercero.color], autoDer[i], finColor);
			if(i!=3)
				printf("\n");
			else
				printf("\n\033[1;100m%-100s\n%-100s%s\n%-100s%s\n", " ", "─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ", "", " ",finColor);
		}
	}else{
        //De derecha a izquierda
		for(i=0; i<4; i++){
			j=0;
			if(p.primero.direccion == -1)
				printf("    %s%6s%s", colores[p.primero.color], autoNulo[i], finColor);
			else
				printf("    %s%6s%s", colores[p.primero.color], autoIzq[i], finColor);
			if(p.segundo.direccion == -1)
				printf("    %s%6s%s", colores[p.segundo.color], autoNulo[i], finColor);
			else
				printf("    %s%6s%s", colores[p.segundo.color], autoIzq[i], finColor);
			if(p.tercero.direccion == -1 )
				printf("    %s%6s%s", colores[p.tercero.color], autoNulo[i], finColor);
			else
				printf("    %s%6s%s", colores[p.tercero.color], autoIzq[i], finColor);
			if(i!=3)
				printf("\n");
			else
				printf("\n\033[1;100m%-100s\n%-100s%s\n%-100s%s\n", " ", "─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ", "", " ",finColor);

		}
	}
	printf("\nCola izquierda: ");
	listar(colaIzq);
	printf("\033[K\nCola derecha: " );
	listar(colaDer);
	printf("\n");
	gotoxy(16,1);

	printf("~:");
	printf("\033[u"); //restaurar posision cursor

	fflush(stdout);
	sleep(SLEEP);
}

int comprobarAuto(char nombre[]){
	int exit=0;
	int i=0;
    char *token;
    const char s[2]=" ";
    token=strtok(nombre,s);	
	token=strtok(NULL,s);
	 	while (nombreAuto[i])
	 	{
	 		if(strcmp(token,nombreAuto[i])==0)
	 		{
              printf("¡El auto ya existe!. Pruebe con otro numero\033[K");
              exit=1;
              return 0;
	 		}
	 		i++;
	 	}
        if(exit==0)
        {
        	nombreAuto[q]=malloc(sizeof(char)*15);
        	strcpy(nombreAuto[q],token);
        	q++;
        	return 1;
        }

}

void *leer(void *str){
    srand(time((NULL)));
    while (quit == 0){
        char cat[LONG];
        char cat2[LONG];
        int i, j = 0, val;
        const char s[2] = " ";
        char *token;
        int check = 1;
        car aux;

        // Lectura de comandos
        gotoxy(16, 1);
        printf("~: ");
        gotoxy(16, 3);
        fflush(stdin);
        scanf(" %[^\n]s", cat);
        fflush(stdin);
        strcpy(cat2, cat);

        token = strtok(cat, s);
        i = 0;
        while (token != NULL)
        {
            j = 0;
            while (token[j] != '\0')
            {
                token[j] = toupper(token[j]);
                j++;
            }
            strcpy(argumentos[i], token);
            token = strtok(NULL, s);
            i++;
        }

        if (strcmp(argumentos[0], "EXIT") == 0)
        {
            pthread_mutex_lock(&msg_mutex);
            quit = 1;
            pthread_mutex_unlock(&msg_mutex);
        }

        while (quit == 0)
        {
            if (strcmp(argumentos[0], "CAR") == 0)
            {
                if (strlen(argumentos[1]) == 6 && (strcmp(argumentos[2], "IZQ") == 0 || strcmp(argumentos[2], "DER") == 0) && i == 3 && (check = comprobarAuto(cat2) == 1))
                {
                    if (primerAuto == 0)
                    {
                        if (strcmp(argumentos[2], "IZQ") == 0)
                            function = 1;
                        else
                            function = 0;
                        primerAuto = 1;
                    }
                    strcpy(aux.nombre, argumentos[1]);

                    pthread_mutex_lock(&msg_mutex);
                        if (strcmp(argumentos[2], "IZQ") == 0)
                        {
                            aux.direccion = 0;
                            aux.color = rand() % 7;
                            encolar(&colaDer, aux);
                        }
                        else
                        {
                            aux.direccion = 1;
                            aux.color = rand() % 7;
                            encolar(&colaIzq, aux);
                        }
                        printf("¡Agregado!. Auto listo para pasar\033[K");
                    pthread_mutex_unlock(&msg_mutex);
                }
                else if (strlen(argumentos[1]) != 6)
                    printf("¡El nombre del auto cuenta con menos de 6 caracteres!\033[K");
                else if (check != 1)
                    check = 1;
                else if (strcmp(argumentos[2], "IZQ") != 0 && strcmp(argumentos[2], "DER") != 0)
                    printf("¡Dirección incorrecta!. Intente de nuevo\033[K");
                else if (i != 3)
                    printf("Ingrese la cantidad de argumentos correctos\033[K");
            }
            else if (strcmp(argumentos[0], "START") == 0)
            {

                if (start == 0)
                {

                    printf("Loading. \033[K"); fflush(stdout);
                    usleep(500000);
                    printf(". "); fflush(stdout);
                    usleep(500000);
                    printf(". "); fflush(stdout);
                    start = 1;
                    usleep(500000);
                    printf("."); fflush(stdout);
                }
                else
                {
                    printf("Mario Kart has started. LP3_Version\033[K");
                }
            }
            else if (strcmp(argumentos[0], "STATUS") == 0)
            {

                if (start == 1)
                {
                    printf("== Lista de autos anciosos por pasar ==\033[K");
                }
                else
                {
                    gotoxy(10, 1);
                    printf("[Left] Espera: ");
                    listar(colaIzq);
                    printf("\033[K\n[Right] Espera: ");
                    listar(colaDer);
                    printf("\033[K\n");
                }
            }
            else
            {
                printf("¡Opción inválida!. Intente de nuevo.\033[K");
                fflush(stdout);
            }

            if (quit == 1)
                break;

            fflush(stdout);
            sleep(1);

            //Borrar mensajes
            printf("\033[1C\033[2K\033[1A");
            fflush(stdout);

            gotoxy(16, 1);
            printf("\33[2K");
            printf("~: ");
            gotoxy(16, 3);
            fflush(stdin);
            scanf(" %[^\n]s", cat);
            fflush(stdin);
            strcpy(cat2, cat);

            token = strtok(cat, s);
            /* recorre los tokens */
            i = 0;
            while (token != NULL)
            {
                j = 0;
                while (token[j] != '\0')
                {
                    token[j] = toupper(token[j]);
                    j++;
                }
                strcpy(argumentos[i], token);
                token = strtok(NULL, s);
                i++;
            }

            if (strcmp(argumentos[0], "EXIT") == 0)
            {
                pthread_mutex_lock(&msg_mutex);
                quit = 1;
                pthread_mutex_unlock(&msg_mutex);
            }
        }
    }
    pthread_exit(0);
}

