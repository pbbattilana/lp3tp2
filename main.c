#include "header.h"

extern int q;
extern car nulo;
extern Puente p;
extern Cola colaIzq;
extern Cola colaDer;
extern char * nombreAuto[100];
extern char * argumentos[10];

int main(int argc, char const *argv[]){
    int h;
	system("clear");
	printf("\n\n\n\n\n\n\n\n");
	for(h=0; h<10; h++)
    	argumentos[h] = malloc(sizeof(char)*15);

    strcpy(nulo.nombre, "      ");
	nulo.direccion = -1;

	crearCola(&colaIzq);
	crearCola(&colaDer);

	crearPuente(&p);

    pthread_t h_leer;
    pthread_t h_iniciar; 

    pthread_create(&h_leer, NULL, leer, NULL);
    pthread_create(&h_iniciar, NULL, iniciar, NULL);

    pthread_join(h_leer, NULL);

    for(h=0; h<10; h++)
    	free(argumentos[h]);
    for(h=0; h<=q; h++)
    	free(nombreAuto[h]);

    return 0;
}