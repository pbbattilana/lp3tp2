### Fecha de defensa: 30/05/2021

### Problema para resolverlo con phtreads, semáforos, mutex y variables de condición si así lo requieran 

En una carretera donde solo existen dos carriles, uno para cada sentido de la circulación
(izquierda a derecha y derecha a izquierda), existe un tramo donde se encuentra un
puente sobre el cual solo pueden pasar coches que vayan en un sentido. Para modelar
este problema, tendremos dos clases de procesos:
1) Coches que van hacia la derecha
2) Coches que van hacia la izquierda
Suponemos que una vez que el coche pasa por el puente (en uno u otro sentido) ya no
vuelve a pasar (llegará a su destino).
Cuando un coche llega al principio del puente, pueden ocurrir tres cosas:
1) Que no pueda pasar porque en el puente ya hay coches que vienen en sentido
contrario y por tanto tendrá que esperar a que el puente esté libre.
2) Que el puente esté libre y entonces puede pasar sin ningún problema.
3) Que el puente no esté libre pero los coches que circulan por él van en el mismo
sentido. En este caso el coche podrá continuar y proseguir su camino cruzando el puente.
Además, para evitar el riesgo de inanición que pueden sufrir los coches de ambos
sentidos debido a que puede ocurrir que siempre haya coches que vienen en sentido
contrario por el puente, se tendrá en cuenta también el siguiente requisito:
- Si hay esperando coches en el otro extremo del puente y ya hay coches accediendo,
cuando hayan pasado 4 coches entonces pasarán el turno a los coches del sentido
contrario que están esperando.
Observaciones:
1) Cada auto tarda 3 segundos en atravesar el Puente, donde durante el segundo “1” está
en la primera parte del Puente, durante el segundo “2” está en el medio del Puente y
durante el segundo “3” está en la parte final del Puente
2) Por tanto, de acuerdo al punto 1) el puente tiene capacidad para 3 autos
simultáneamente y puede verse como un “pipeline”Los comandos para interactuar con el programa de manera son los siguientes:

**car izq**

- agrega un auto con id “autoNN” en el sentido que va hacia la izquierda, donde N es un
numero autogrenerado es decir el contador de autos que vamos agregando (01,02,03, ….
etc)
- sin salida en pantalla
- nombre debe ser de 6 caracteres obligatoriamente

**car der**

- agrega un auto con id “autoNN” en el sentido que va hacia la derecha, donde N es el
mismo contador explicado en el punto anterior
- sin salida en pantalla

**status**

- qué autos están en espera para atravezar el Puente e ir hacia la derecha
- qué autos están en espera para atravezar el Puente e ir hacia la izquierda

**start**

- comando que arranca la simulación (los que van hacia la derecha arrancan ingresando al
puente) e imprime su estado actual:
- qué autos están en el puente
- qué autos están en espera para atravezar el Puente e ir hacia la derecha
- qué autos están en espera para atravezar el Puente e ir hacia la izquierda
- cursor para ejecutar el comando “car”, es decir, se debe poder seguir agregando
autos en tiempo real una vez que haya comenzado la simulación
Esta salida debe refrescarse cada 1 segundo.
Ejemplo de salida para este comando (es solo un ejemplo que puede adoptarse como
salida, aunque cada uno puede hacerlo a su manera pero debe ser claro y preciso para
poder verificar el correcto funcionamiento del simulador):

==================================================================
================>>>>>>>>>>======>>>>>>>>>>=======>>>>>>>>>>=======
================>>auto01>>======>>auto02>>=======>>auto03>>=======
================>>>>>>>>>>======>>>>>>>>>>=======>>>>>>>>>>=======
==================================================================

** Autos en espera para atravesar el puente **
=> auto04
=> auto05
=> auto06
=> auto07
<= auto08
<= auto09
<= auto10
<= auto11
<= auto12
=> auto13
<= auto14

> car _ _
